package com.example.springbootwithreactjs.controller;

import com.example.springbootwithreactjs.model.User;
import com.example.springbootwithreactjs.repository.UserRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;

/**
 */
@RestController
public class UsersController {

  @RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @CrossOrigin(origins = "http://localhost:3000")
  public List<User> users() {
    UserRepository repository = new UserRepository();
    return repository.getUsers();
  }
}
