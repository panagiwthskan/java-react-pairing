package com.example.springbootwithreactjs.repository;

import com.example.springbootwithreactjs.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    public List<User> getUsers() {
        final User firstUser = new User('1', "Mike");
        final User secondUser = new User('2', "Tim");
        final User thirdUser = new User('3', "Sven");

        ArrayList<User> list = new ArrayList<>();
        list.add(firstUser);
        list.add(secondUser);
        list.add(thirdUser);
        return list;
    }
}
