import React, { Component } from "react";
import "../css/App.css";
import axios from "axios";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = { users: [] };
  }

  submit = () => {
    axios.get("http://localhost:8080/users").then(
      res => {
        this.setState({users: res.data});
        console.log(res.data);
      },
      err => {
        alert("Server rejected response with: " + err);
      }
    );
  };

  render() {
    const users = this.state.users.map((user) => {
      return (<div key={user.id}>{user.name}</div>);
    })

    return (
      <div className="Main">
        <header className="App-header">
          <h1 className="App-title">Ping</h1>
        </header>
        <p className="App-intro">
          <div>
            <button onClick={this.submit}>Load Users</button>
            <div>Users: {users} </div>
          </div>
        </p>
      </div>
    );
  }
}

export default Main;
